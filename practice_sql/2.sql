select * FROM
((SELECT *, 'post' as `type` from `posts`)
union all
(select *, 'news' as `type` from `news`)) as `union_table`
order by `union_table`.`date`
limit 10;