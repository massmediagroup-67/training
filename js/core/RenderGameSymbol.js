/*
рендерить графіку в canvas по id залежно вiд data
  */

class RenderGameSymbol {
  renderStep(id, data) {
    const canvas = document.getElementById(id);
    const ctx = canvas.getContext('2d');
    const img = new Image();

    img.onload = function () {
      ctx.drawImage(img, 0, 0, 300, 150);
    };
    if (data === 'x') {
      img.src = './Resources/img/x.png';
    } else if (data === '0') {
      img.src = './Resources/img/0.png';
    }
    $(`#${id}`).off('click');
  }

  clean() {
    for (let i = 0; i < 9; i += 1) {
      const canvas = document.getElementById(i);
      const ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
  }
}

export default RenderGameSymbol;