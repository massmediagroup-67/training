/*
Відповідає за аналіз кроків (ходов)
 */

class Game {
  constructor(resultMap, iterator = 0) {
    this.resultMap = resultMap; // карта гри
    this.iterator = iterator; // крок
  }

  // make game step by map id (see map id list in ResultMap.js)
  makeMove(id) {
    if (id < 0 || id > 8) return false;
    if (this.iterator % 2 === 0) {
      this.resultMap.changeMap('x', id);
      if (this.resultMap.checkWinner()) return 'X';
    } else {
      this.resultMap.changeMap('0', id);
      if (this.resultMap.checkWinner()) return '0';
    }
    this.iterator += 1;
    if (this.iterator > 8) return 'none';
    $.session.set('iterator', this.iterator);
    return false;
  }

  reset() {
    this.resultMap.clean();
    this.iterator = 0;
    $.session.clear();
  }
}

export default Game;
