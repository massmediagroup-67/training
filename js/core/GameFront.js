class GameFront {
  constructor(game, render, messageSelector) {
    this.game = game;
    this.render = render;
    this.message = $(messageSelector);
  }

  startGame() {
    for (let i = 0; i < 9; i += 1) {
      const canvas = $(`#${i}`);
      if ($._data(document.getElementById(i), 'events') === undefined) {
        canvas.on('click', () => {
          this.makeStep(i);
        });
      }
    }
  }

  makeStep(i) {
    if (this.game.iterator % 2 === 0) {
      this.render.renderStep(i, 'x');
      this.message.html('<h1> Ходит 0</h1>');
    } else {
      this.render.renderStep(i, '0');
      this.message.html('<h1> Ходит X</h1>');
    }

    const result = this.game.makeMove(i);

    if (result) {
      result === 'none' ? this.endGame('Ничия') : this.endGame(`Победитель ${result}`);
    }
  }

  endGame(message) {
    // delete click handlers in order to ban make steps
    for (let i = 0; i < 9; i += 1) {
      $(`#${i}`).off('click');
    }
    const messageDiv = $('.message');
    messageDiv.empty();
    messageDiv.append(`<h1> ${message}</h1>`);
    const button = $('#restart');
    button.css('display', 'inline-block');
    // make restart button
    button.on('click', () => {
      this.game.reset();
      button.css('display', 'none');
      this.render.clean();
      messageDiv.empty();
      messageDiv.append('<h1> Ходит Х </h1>');
      this.startGame();
    });
  }
}

export default GameFront;
