/*
Map id list

 map[9]:
     position:
         0 1 2
         3 4 5
         6 7 8

 */

class ResultMap {
  constructor() {
    this.map = [];
  }

  get gameMap() {
    return this.map;
  }

  set gameMap(map) {
    if (map.length > 9) return;
    this.map = map;
    $.session.set('map', map);
  }

  static checkEl(el) {
    if (!(el === 'x' || el === '0')) {
      return false;
    }
    return true;
  }

  // set new data to position
  changeMap(data, position) {
    if (position >= 0 && position < 9 && ResultMap.checkEl(data)) {
      const map = this.gameMap;
      map[position] = data;
      this.gameMap = map;
    }
  }

  clean() {
    this.gameMap = [];
  }

  checkWinner() {
    if (this.map.length < 3) return false;

    for (let i = 0; i < 3; i += 1) {
      if (this.checkLine(i)) return true;
    }

    for (let i = 0; i < 3; i += 1) {
      if (this.checkColumn(i)) return true;
    }

    for (let i = 0; i < 2; i += 1) {
      if (this.checkDiagonal(i)) return true;
    }

    return false;
  }

  checkLine(line) {
    const i = line * 3;
    return this.map[i] === this.map[i + 1]
        && this.map[i + 1] === this.map[i + 2]
        && ResultMap.checkEl(this.map[i]);
  }

  checkColumn(column) {
    return this.map[column] === this.map[column + 3]
        && this.map[column + 3] === this.map[column + 6]
        && ResultMap.checkEl(this.map[column]);
  }

  checkDiagonal(diagonal) {
    if (diagonal === 0
        && this.map[0] === this.map[4]
        && this.map[4] === this.map[8]
        && ResultMap.checkEl(this.map[0])) return true;

    return diagonal === 1
        && this.map[2] === this.map[4]
        && this.map[4] === this.map[6]
        && ResultMap.checkEl(this.map[2]);
  }
}

export default ResultMap;
