import Game from './core/Game.js';
import ResultMap from './core/ResultMap.js';
import RenderGameSymbol from './core/RenderGameSymbol';
import GameFront from './core/GameFront';

const session = require('jquery.session');

const resultMap = new ResultMap();
const render = new RenderGameSymbol();

let game = new Game(resultMap);

if ($.session.get('iterator') !== undefined) {
  game = new Game(resultMap, Number($.session.get('iterator')));
  let message;
  if ($.session.get('iterator') % 2 === 0) {
    message = 'Ходит Х';
  } else {
    message = 'Ходит 0';
  }
  $('.message').append(`<h1> ${message} </h1>`);
} else {
  game = new Game(resultMap);
  $('.message').append('<h1> Ходит Х </h1>');
}

const gameFront = new GameFront(game, render, '.message');
// load steps from session
if ($.session.get('map') !== undefined) {
  resultMap.map = $.session.get('map').split(',');
  resultMap.map.forEach((i) => {
    if (resultMap.map.hasOwnProperty(i) && ResultMap.checkEl(resultMap.map[i])) {
      render.renderStep(i, resultMap.map[i]);
    }
  });
  if (game.resultMap.checkWinner()) game.iterator % 2 === 0 ? gameFront.endGame('Победитель X') : gameFront.endGame('Победитель 0');
}

gameFront.startGame();
